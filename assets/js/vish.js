
const vishWrapp = document.querySelector('.vishlist-card-items');

function vishGetItems() {
    fetch('https://basova.top/api/cards')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      data.forEach((card) => {
        let cart =  JSON.parse(localStorage.getItem('vish'));
    
        if(cart !== null) {
            cart.forEach((cartItem) => {
            if (cartItem.id == card.id) {
                let item =  createFIllCart(card, cartItem);
                vishWrapp.appendChild(item);
            }
            })
        } 
      })
  
    });
  }


  vishGetItems()

  export function addToVish(id) {
    console.log(id);
  
  let card = JSON.parse(localStorage.getItem('vish'));
  
  if(card === null) {
    card = [];
  }
  
  let inCart = false;
  
  if(card.length > 0) {
    card.forEach( (item, index) => {
      if(item.id == id) {
        inCart = true
      }
    })
  }
  
  if(!inCart) {
    let basket = {
      id: id, 
      count : 1
    }
  
    card.push(basket)
  }
  
  
  localStorage.setItem('vish', JSON.stringify(card));
}


function createFIllCart(card, cardItem) {
    
    let vishlistCardItem = document.createElement('div');
    vishlistCardItem.classList.add('vishlist-card-item');
    vishWrapp.appendChild(vishlistCardItem);

    let vishlistCardIimg = document.createElement('div');
    vishlistCardIimg.classList.add('vishlist-card-img');
    vishlistCardItem.appendChild(vishlistCardIimg);

    let img = document.createElement('img');
    img.src = card.image;
    vishlistCardIimg.appendChild(img);


    let vishRating = document.createElement('div');
    vishRating.classList.add('vish-rating');
    vishlistCardItem.appendChild(vishRating)

    let rating= document.createElement('div');
    rating.classList.add('rating');
    rating.innerHTML = `
    <input type="radio" id="star5" name="rating" value="5" />
    <label for="star5" title="text">5 stars</label>
    <input type="radio" id="star4" name="rating" value="4" />
    <label for="star4" title="text">4 stars</label>
    <input type="radio" id="star3" name="rating" value="3" />
    <label for="star3" title="text">3 stars</label>
    <input type="radio" id="star2" name="rating" value="2" />
    <label for="star2" title="text">2 stars</label>
    <input type="radio" id="star1" name="rating" value="1" />
    <label for="star1" title="text">1 star</label>`
    vishRating.appendChild(rating);


    let stock = document.createElement('div');
    stock.classList.add('stock');
    stock.innerHTML = 'В наявності'
    vishRating.appendChild(stock);

    let vishlistCardName = document.createElement('div');
    vishlistCardName.classList.add('vishlist-card-name');
    vishlistCardName.innerHTML = card.name
    vishlistCardItem.appendChild(vishlistCardName);

    let price = document.createElement('div');
    price.classList.add('price');
    price.innerHTML = `${card.price} грн`;
    vishlistCardItem.appendChild(price)

    let vishBtnWwrapp = document.createElement('div');
    vishBtnWwrapp.classList.add('vish-btn-wrapp');
    vishlistCardItem.appendChild(vishBtnWwrapp)

    let buttonVish = document.createElement('div');
    buttonVish.classList.add('button-vish');
    buttonVish.innerHTML = `В кошик`
    vishBtnWwrapp.appendChild(buttonVish);


    let deleteBtn = document.createElement('div');
    deleteBtn.classList.add('delete');
    deleteBtn.innerHTML = `Видалити`
    vishBtnWwrapp.appendChild(deleteBtn);




    return vishlistCardItem;
}


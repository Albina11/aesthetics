const submitBtn = document.querySelector('.submit-btn');
const contactsform = document.querySelector('.contacts-form');
const contactsThanks =  document.querySelector('.contacts-thanks');
submitBtn.addEventListener('click', () => {
    contactsform.classList.add('hidden')
    contactsThanks.classList.add('show');
});
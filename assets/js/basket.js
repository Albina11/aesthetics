// import { createCard, addToCard } from "./cards";
// import data from './card.json' assert { type: 'json' };
const bascketWrapp = document.querySelector('.inner-wrap');

let summPrice = document.createElement('div');
summPrice.classList.add('price');
bascketWrapp.appendChild(summPrice);
let span = document.createElement('span')
summPrice.appendChild(span);
span.innerHTML = ` Загальна сумма: 2800 грн`

function orderGetItems(filters = []) {
  let queryPar = {};
  filters.forEach((item) => {
    queryPar[item.type] = item.param
  })

  fetch('https://basova.top/api/cards?' + new URLSearchParams(queryPar))
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    data.forEach((card) => {
      let cart =  JSON.parse(localStorage.getItem('card'));
  
      if(cart !== null) {
          cart.forEach((cartItem) => {
          if (cartItem.id == card.id) {
              let item =  createFIllCart(card, cartItem);
              bascketWrapp.appendChild(item);
          }
          })
      } 
    })

  });
}


orderGetItems()

function createFIllCart(card, cardItem) {
    
    let basketCardItem = document.createElement('div');
    basketCardItem.classList.add('basket-card-item');
    basketCardItem.dataset.id = card.id


    let bascketImgWrapp = document.createElement('div');
    bascketImgWrapp.classList.add('basket-card-img')
    let bascketImg = document.createElement('img')
    bascketImg.classList.add('basket-img')
    bascketImg.src = card.image;

    let bascketCardName = document.createElement('div');
    bascketCardName.classList.add('basket-card-name');
    bascketCardName.innerText = card.name

    let bascketCount = document.createElement('div');
    bascketCount.classList.add('basket-card-count');
    let input = document.createElement('input');
    input.classList.add('product-count')
    input.type = 'text'
    input.min = 1;
    input.value = cardItem.count
    let up = document.createElement('img');
    let down = document.createElement('img');
    up.classList.add('img-arrow')
    up.src = `./assets/img/Property1=default_up.png`
    down.classList.add('img-arrow')
    down.src = `./assets/img/Property1=default_down.png`

    up.addEventListener('click', (e) => {
      let countWrapp =  up.closest('.basket-card-count')
      let count = countWrapp.querySelector('.product-count');
      count.value = incremenItemCount(cardItem.id, count.value)
    })

    down.addEventListener('click', (e) => {
      let countWrapp =  down.closest('.basket-card-count')
      let count = countWrapp.querySelector('.product-count');
      count.value = decremenItemCount(cardItem.id, count.value);
    })

    let imgWrapp = document.createElement('div');
    imgWrapp.classList.add('wrapper-img');
 
    bascketCount.appendChild(imgWrapp)
    imgWrapp.appendChild(up)
    imgWrapp.appendChild(down)
   


    let cardCost = document.createElement('div');
    cardCost.classList.add('basket-card-cost');
    cardCost.innerText = `${card.price} грн`


    let deleteCard = document.createElement('div');
    deleteCard.classList.add('basket-card-delete')

    deleteCard.addEventListener('click', (e) => {
      removeFromCart(deleteCard.closest('.basket-card-item').dataset.id)
      deleteCard.closest('.basket-card-item').remove()
    })

    let deleteImg = document.createElement('img')
    deleteImg.classList.add('delete-img')
    deleteImg.src = `./assets/img/delete=default.png`

  

    basketCardItem.appendChild(bascketImgWrapp);
    bascketImgWrapp.appendChild(bascketImg)
    basketCardItem.appendChild(bascketCardName)
    basketCardItem.appendChild(bascketCount)
    // bascketCount.appendChild(up)
    // bascketCount.appendChild(down)
    bascketCount.appendChild(input)
    basketCardItem.appendChild(cardCost)
    basketCardItem.appendChild(deleteCard)
    deleteCard.appendChild(deleteImg)
    
    return basketCardItem;
}

function removeFromCart(id) {
  console.log(id);
  let card = JSON.parse(localStorage.getItem('card'));
  if(card.length > 0) {
    card.forEach( (item, index) => {
      if(item.id == id) {
        card.splice(index, 1)
      }
    })
  }
  localStorage.setItem('card', JSON.stringify(card));
}

function incremenItemCount (productId, count) {
  let card = JSON.parse(localStorage.getItem('card'));
  if(card.length > 0) {
    card.forEach( (item, index) => {
      if(item.id == productId) {
        item.count = ++count;
        card[index] = item
      }
    })
  }
  localStorage.setItem('card', JSON.stringify(card));
  return count;
}


function decremenItemCount (productId, count) {
  let card = JSON.parse(localStorage.getItem('card'));
  if(card.length > 0) {
    card.forEach( (item, index) => {
      if(item.id == productId) {
        if(count > 1){
          count--;
        } else {
          count = 1
        }

        item.count = count
        card[index] = item
      }
    })
  }
  localStorage.setItem('card', JSON.stringify(card));
  return count;
}
import { addToVish } from "./vish.js";
const productCard = document.querySelector('.product-card');



async function fetchCards(filters) {
  try {

      let queryPar = {};
        filters.forEach((item) => {
          queryPar[item.type] = item.param
        })

        fetch('https://basova.top/api/cards?' + new URLSearchParams(queryPar))
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          productCard.innerHTML = '';
          for (let key in data) {
            let item =  createCard(data[key]);
            productCard.appendChild(item);
          }

        });
  } catch (error) {
    console.error(error);
  }
}


 function createCard(card) {

   let item = document.createElement('div');
    item.classList.add('product-item');
    let itemLink = document.createElement('a');
    let productItemImg = document.createElement('div');
    productItemImg.classList.add('product-item-img');
    productItemImg.style.backgroundImage = `url(${card.image})`;
    itemLink.appendChild(productItemImg);

    let vish = document.createElement('div');
    vish.classList.add('vish')
    vish.dataset.id = card.id
    vish.addEventListener('click' , (e) => {
      addToVish(e.target.closest('.vish').dataset.id);
    })
    productItemImg.appendChild(vish)

    let likeImg = document.createElement('span');
    likeImg.classList.add('like-img');
    vish.appendChild(likeImg);

    let svg = document.createElement('svg');
    svg.classList.add('icon');
    svg.innerHTML = `<svg width="32" height="30" viewBox="0 0 32 30"  xmlns="http://www.w3.org/2000/svg">
    <g clip-path="url(#clip0_1123_975)">
    <path d="M8.91536 1.25C5.00395 1.25 1.83203 4.38933 1.83203 8.2625C1.83203 11.3891 3.07161 18.8096 15.2734 26.3108C15.4919 26.4438 15.7429 26.5142 15.9987 26.5142C16.2545 26.5142 16.5055 26.4438 16.724 26.3108C28.9258 18.8096 30.1654 11.3891 30.1654 8.2625C30.1654 4.38933 26.9934 1.25 23.082 1.25C19.1706 1.25 15.9987 5.5 15.9987 5.5C15.9987 5.5 12.8268 1.25 8.91536 1.25Z" stroke="#4B6049" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>`;
    likeImg.appendChild(svg);

    let productName = document.createElement('div');
    productName.classList.add('product-name');
    productName.innerHTML = card.name;
    itemLink.appendChild(productName);

    let productPrice = document.createElement('div');
    productPrice.classList.add('product-price');
    productPrice.innerHTML = `${card.price} грн` ;
    itemLink.appendChild(productPrice);


    let  moreLink = document.createElement('div');
    moreLink.classList.add('more-link');
    itemLink.appendChild(moreLink);
   

    let cardLink = document.createElement('a');
    cardLink.classList.add('card-link');
    cardLink.href = `./card-product.html?product=`+ card.id;
    cardLink.innerText = `Детальніше`;
    moreLink.appendChild(cardLink);

    let  btnBascketWrapp = document.createElement('div');
    btnBascketWrapp.classList.add('btn-wrapp-in-bascket');

    let  catalogBtnBascket = document.createElement('div');
    catalogBtnBascket.classList.add('catalog-btn-in-bascket');
    catalogBtnBascket.dataset.id = card.id
    catalogBtnBascket.innerText = `В кошик`;


  let cart =  JSON.parse(localStorage.getItem('card'));

    if(cart !== null) {
      cart.forEach((item) => {
       if (item.id == card.id) {
        catalogBtnBascket.innerText = `+`;
       }
      })
    } 
    

    catalogBtnBascket.addEventListener('click' , (e) => {
      console.log(e);
     addToCard(e.target.dataset.id);
      e.target.innerText = `+`
    })


    btnBascketWrapp.appendChild(catalogBtnBascket);
    itemLink.appendChild(moreLink);

    moreLink.appendChild(btnBascketWrapp);

    item.appendChild(itemLink);
    return item;

}


export function addToCard(id) {
  console.log(id);

let card = JSON.parse(localStorage.getItem('card'));

if(card === null) {
  card = [];
}

let inCart = false;

if(card.length > 0) {
  card.forEach( (item, index) => {
    if(item.id == id) {
      inCart = true
    }
  })
}

if(!inCart) {
  let basket = {
    id: id, 
    count : 1
  }

  card.push(basket)
}


localStorage.setItem('card', JSON.stringify(card));



}




function filter(param) {
  const filter = document.querySelectorAll('.filter-item .filters .item-input');



  filter.forEach((item) => {

    item.addEventListener('click', (e)=> {
      let  type = e.target.dataset.type;
      let  param = e.target.dataset.id;
      
      let filters = JSON.parse(localStorage.getItem('filters'));

      if(filters === null) {
        filters = [];
      }
      if(filters.length > 0) {
        filters.forEach( (filter, index) => {
          if(filter.type == type) {
            console.log(filter)
            if(!filter.param.includes(param)) {
              filter.param.push(param)
            }
          } else {
            filters.push({
              type:type,
              param:[param]
            })
          }
        })
      } else {
        filters.push({
          type:type,
          param:[param]
        })
      }

      localStorage.setItem('filters', JSON.stringify(filters));
      console.log(filters);
        console.log(type);
        console.log(param);

        fetchCards(filters)
      })
  })
}

sendRequest();

function sendRequest() {
    let params = (new URL(document.location)).searchParams;
    let product_id = params.get("product");

    fetch('https://basova.top/api/card?product=' + product_id)
    .then((response) => {
    return response.json();
    })
    .then((data) => {
        console.log(data)

    document.querySelector('.product-description h3').innerHTML = data.name
    document.querySelector('.card-product-container .description').innerHTML = data.description
    document.querySelector('.product-img-item').src = data.image
    document.querySelector('.pr').innerHTML = `${data.price} грн`
    

});
}

fetchCards([])
filter();